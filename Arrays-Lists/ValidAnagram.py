def validAnagram(s: str, t: str) -> bool:

    if len(s) != len(t):
        return False

    countS, countT = {}, {}
    # Creating 2 Dictionaries to store the occurences of each letter

    #Loop through the length of s
    for i in range(len(s)):
        countS[s[i]] = 1 + countS.get(s[i], 0)
        countT[t[i]] = 1 + countT.get(t[i], 0)

    for c in countS:
        if countS[c] != countT.get(c, 0):
            return False

    return True


print(validAnagram("anagram", "nagaram"))



        # If the key in countS doesn't exist, make a new key and increment by 1
