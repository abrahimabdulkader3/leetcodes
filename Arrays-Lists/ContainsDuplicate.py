def containsDuplicate(self, nums: List[int]) -> bool:
    listSet = set()

    for num in nums:
        if num in listSet:
            return True

        else:
            listSet.add(num)

    return False
